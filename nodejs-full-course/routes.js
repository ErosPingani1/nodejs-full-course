const fs = require('fs');

const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if (url === '/') {
        res.write('<html>');
        res.write('<head><title>Enter Message</title></head>');
        res.write('<body><form action="/message" method="POST"><input name="message" type="text"/><button type="submit">Send</button></form></body>');
        res.write('</html>');
        return res.end(); // The response must always be returned with return
    }
    if (url === '/message' && method === 'POST') {
        const body = []; // Chunks of data must be stored in an array
        // on events are asynchronous and so are executed after all the code inside the url request
        req.on('data', (chunk) => { // Event fired when a chunk of data is compiled in the stream
            console.log(chunk);
            body.push(chunk);
        });
        return req.on('end', () => { // Event on stream end, when all the data is returned
            const parsedBody = Buffer.concat(body).toString(); // Through buffer received chunks can be converted in strings
            const message = parsedBody.split('=')[1];
            // WriteFile is different from WriteFileSync because asynchronous, and so it has a callback to launch when completed
            fs.writeFile('message.txt', message, err => {
                console.log(parsedBody);
                res.statusCode = 302;
                res.setHeader('Location', '/');
                return res.end(); // The response must always be returned with return
            });
        });
    }
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>My First Page</title></head>');
    res.write('<body><h1>Hello from my Node.js Server!</h1></body>');
    res.write('</html>');
    res.end();
}

module.exports = requestHandler;

// Possible ways to export properties from routes.js file
// module.exports = {
//     handler: requestHandler,
//     sometText: 'Some hard coded text'
// }

// exports.handler = requestHandler;
// exports.sometText = 'Some hard coded text';