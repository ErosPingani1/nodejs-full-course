/* Find all true values in bool array (if empty return 0) */
const countTrue = arr => {
    return arr.length > 0 ? arr.filter(val => val).length : 0;
}

/* Find minimum and maximum values in array and print them in order */
const minMax = arr => {
    return [ Math.min(...arr), Math.max(...arr) ];
}