const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if (url === '/') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<header><title>First Assignement</title></header>');
        res.write('<body><h1>This is an amazing first assignement!</h1><form action="/create-user" method="POST"><input name="user" type="text"/><button type="submit">Add User</button></form></body>');
        res.write('</html>');
        return res.end();
    } else if (url === '/users') {
        res.setHeader('Content-Type', 'text/html');
        res.write('<html>');
        res.write('<body><ul><li>Mario</li><li>Luigi</li><li>Marcello</li></ul></body>');
        res.write('</html>');
        return res.end();
    }
    else if (url === '/create-user' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        });
        req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            console.log(parsedBody.split('=')[1]);
        });
        res.statusCode = 302;
        res.setHeader('Location', '/');
        res.end();
    }
}

module.exports = requestHandler;