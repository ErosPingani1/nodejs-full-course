const express = require('express');

const app = express();

// Functions executed one after the other
// app.use((req, res, next) => {
//     console.log('This is the first beautiful function!');
//     next();
// });

// app.use((req, res, next) => {
//     console.log('This is the second beautiful function!');
//     res.send('<h1>This is the end.</h1>');
// });

app.use('/users', (req, res, next) => {
    res.send('<h1>No users available, ad required.</h1>')
});

app.use('/', (req, res, next) => {
    res.send('<h1>This is not the end, this is just the beginning (ciaone).</h1>')
});

app.listen(3000);